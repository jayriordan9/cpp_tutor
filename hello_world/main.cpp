/*
 *  This is a single file program to print the string:
 *  "Hello World!"
 *
 */

 /* multi-line comments begin with a /* and end with a */

//Single line comments begin with a //

//This gets us access to cout
#include <iostream>

//main is a special function name, this tells the operating system where to 
//begin executing your program
//main takes two arguments, argc being argument count, or how many arguments
//are being passed to this program, and argv who holds a reference to the 
//arguments.  Don't be too concerned with this just yet, just understand that
//the arguments exist.  main also returns an integer back to the operating
//system.  This could mean something to whoever launched the application.

//retrn type    function name   function arguments
int             main            (int argc, char **argv)
{
  //This is where we print out our string and a new line
  std::cout << "Hello World!" << std::endl;

  //We're returning the constant integer value 0.
  return 0;
}

