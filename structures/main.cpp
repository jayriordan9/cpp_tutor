
#include <iostream>
#include <string>

//Data structures are very important in programming.
//These allow us to group similar data, or group data in a meaningful way

//typedef tells the compiler we're defining a type here.  The syntax isn't
//actually completed on this first line but ends at the end of the structure
//definition.  We're defining a struct Person, but we're defining a type
//Person_t as a struct Person.  Subtle detail here but it's syntax sugar for
//not writing "struct Person" everywhere 
typedef struct Person
{
  std::string name;
  int age; //in years
  double height;
  double weight;

}Person_t;

//
//
//
//
//

int main(int argc, char **argv)
{
  //We could declare p like this, but that's more typing so we'll use Person_t
  //struct Person p;
  Person_t p;

  //Memory isn't initalized by itself, we must do it.  But, we're going to use
  //it right away and give it meaningful data.
  p.name = "Joe";
  p.age  = 15;
  p.height = 145;
  p.weight = 200;

  std::cout << "  Name: " << p.name   << std::endl;
  std::cout << "   Age: " << p.age    << std::endl;
  std::cout << "Height: " << p.height << std::endl;
  std::cout << "Weight: " << p.weight << std::endl;

  return 0;
}


