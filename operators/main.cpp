/*
 *
 *  This program will teach you about the different operators,
 *  functions, and comparisons.  Do you notice anything strange 
 *  about integer division?
 *
 */

#include <iostream>

//RULE: functions must be declared in the file above where they're used!

//function declaration for addition
int add(int a, int b);

//function declaration for subtraction
int subtract(int a, int b);

//function declaration for multiply
int multiply(int a, int b);

//function declaration for divide
int divide(int a, int b);

//function declaration for modulous
int modulous(int a, int b);

//function declaration for AND 
bool AND(bool a, bool b);

//function declaration for OR
bool OR(bool a, bool b);

//
//
//
//
//

int main(int argc, char **argv)
{
  //Call our add function and print the result
  std::cout << "5 + 3 = " << add(5, 3) << std::endl;

  //Call our subtract function and print the result
  std::cout << "5 - 3 = " << subtract(5, 3) << std::endl;

  //Call our multiply function and print the result
  std::cout << "5 * 3 = " << multiply(5, 3) << std::endl;

  //Call our divide function and print the result
  std::cout << "5 / 3 = " << divide(5, 3) << std::endl;

  //Call our modulous function and print the result
  std::cout << "9 % 2 = " << modulous(9, 2) << std::endl;

  //Call our AND function and print the result
  std::cout << "true AND false = " << AND(true, false) << std::endl;
  std::cout << "true AND true  = " << AND(true, true) << std::endl;

  //Call our OR function and print the result
  std::cout << "true OR true   = " << OR(true, true) << std::endl;
  std::cout << "true OR false  = " << OR(true, false) << std::endl;
  std::cout << "false OR false = " << OR(false, false) << std::endl;
  
  return 0;
}

//
//
//
//
//

//function definition for addition
int add(int a, int b)
{
  //Addition operator
  return a + b;
}

//
//
//
//
//

int subtract(int a, int b)
{
  //Subtraction operator
  return a - b;
}

//
//
//
//
//

int multiply(int a, int b)
{
  //multiplication operator
  return a * b;
}

//
//
//
//
//

int divide(int a, int b)
{
  //comparison and the not-equal operator
  if(b != 0)
    //Division operator
    return a / b;
  
  return 0;
}

//
//
//
//
//

int modulous(int a, int b)
{
  //The modulous operator gives us the remainder of the division
  return a % b;
}

//
//
//
//
//

bool AND(bool a, bool b)
{
  return a && b;
}

//
//
//
//
//

bool OR(bool a, bool b)
{
  return a || b;
}




