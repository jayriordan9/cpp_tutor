
/*
 *  This program will teach you about multiple file projects
 *  and classes
 *
 */

#include <iostream>

//In the file above where we're using it, provide a definition for vector
//This is a container class and is part of the standard template library (STL)
#include <vector>

//We've written the definition of the class Person in person.h,
//because we're using it we need to include a definition in the file above 
//where we use it
#include "person.h"

//syntax sugar to allow us to call cout and endl without using std::cout and
//std::endl
using namespace std;

int main(int argc, char **argv)
{
  //Here we declare a vector of Person's
  //Vector is a template class, meaning it can be defined for any type,
  //here we're using type Person.  The vector is created empty.
  vector<Person> People;

  //Delcare p as an object who is of class Person
  Person p;

  //Using the member functions of class Person,
  //define some specific details
  p.setName("Joe");
  p.setAge(5);
  p.setHeight(56.3);
  p.setWeight(40.9);

  //People is our container, it holds Person's.  push_back is a member function
  //of vector that tells the vector to take the value we pass it and store it 
  //at the back of the container
  People.push_back(p);

  //Using the same Person p as before, describe another the details of another
  p.setName("Fred");
  p.setAge(42);
  p.setHeight(156);
  p.setWeight(200);

  //Call push_back again on p, and People will copy p onto the back of the vector
  People.push_back(p);

  //Lets show what's in our vector of person's
  for(auto tempPerson : People)
  {
    //We defined show person to print out the values in the object
    tempPerson.showPerson();
    cout << endl;
  }

  return 0;
}


