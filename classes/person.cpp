
#include "person.h"
#include <iostream>

//
//
//
//
//

//The definition of the constructor for person.  This is called automatically
//when the object is created; therefore it is a great time to set initial 
//conditions and initialize to a known state.
Person::Person()
{
  _age = 0;
  _name = "default";
  _height = 0;
  _weight = 0;
}

//
//
//
//
//

//Destructor definition.  Is there anything we need to do before the memory is 
//cleaned up and handed back to the system?
Person::~Person()
{

}

//
//
//
//
//

void Person::setName(string name)
{
  _name = name;
}

//
//
//
//
//

string Person::getName(void) const
{
  return _name;
}

//
//
//
//
//

void Person::setAge(int age)
{
  _age = age;
}

//
//
//
//
//

int Person::getAge(void) const
{
  return _age;
}

//
//
//
//
//

void Person::setHeight(double height)
{
  _height = height;
}

//
//
//
//
//

double Person::getHeight(void) const
{
  return _height;
}

//
//
//
//
//

void Person::setWeight(double weight)
{
  _weight = weight;
}

//
//
//
//
//

double Person::getWeight(void) const
{
  return _weight;
}

//
//
//
//
//

void Person::showPerson(void) const
{
  std::cout << "Person: " << _name << std::endl;
  std::cout << "   age: " << _age << std::endl;
  std::cout << "height: " << _height << std::endl;
  std::cout << "weight: " << _weight << std::endl;
}

