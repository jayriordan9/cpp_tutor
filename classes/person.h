
#ifndef _PERSON_H_
#define _PERSON_H_

//In the file above where we use string, we define it
#include <string>

//Syntax sugar to avoid typing std::cout etc all over
using namespace std;


//We're defining a class called Person and this is the definition
//Classes are a lot like structures, and this should look familiar from the 
//structures example.  Classes have functions that are automatically called 
//when the memory for them is allocated, and they can provide more than member
//variables.  They can provide member functions; and members can have their 
//access limited
class Person
{
//Public label tells the compiler that these members can be accesses by anyone
//without restriction
public:
  //Declaration of the constructor for class Person,
  //this is called (automatically) when the memory is allocated for an object
  Person();

  //Declaration of the destructor for class Person,
  //this is called (automatically sometimes) when the memory is freed
  ~Person();

  //Member functions that can modify the objects memory
  //
  //This is the first time we're seeing the keyword const.
  //This tells anyone calling this function that by calling this function,
  //we're not going to change any of the values of the object.
  //If you try to modify a variable in the definition of this function with
  //const the compiler will give you an error
  //
  //const should be used wherever possible, the compiler will treat
  //functions differently if they're labeled const and you will convey the
  //intent much clearer
  void setName(string name);
  string getName(void) const;

  void setAge(int age);
  int getAge(void) const;

  void setHeight(double height);
  double getHeight(void) const;

  void setWeight(double weight);
  double getWeight(void) const;

  void showPerson(void) const;

//Private label tells the compiler that these members can only be accessed
//by the object itself and not by outsiders
private:
  //Member variables for class Person.
  //Each object will have its own memory for these variables and 
  //they can only be modified by functions defined in this class
  string _name;
  int    _age; //in whole years
  double _height; //in cm
  double _weight; //in kg

};


#endif
