
/*
 *  This program will provide examples of loops
 *
 */

#include <iostream>

void for_loop(void);
void while_loop(void);
void do_while_loop(void);
void infinite_loop(void);


int main(int argc, char **argv)
{
  for_loop();
  while_loop();
  do_while_loop();
  infinite_loop();

  return 0;
}

//
//
//
//
//

void for_loop(void)
{
  //c style for loop, the iterator must be declared first
  int iter = 0;

  for(iter = 0; iter < 10; iter++)
  {
    //Will the code when iter == 10 ever run?
    if(iter == 10)
      std::cout << "C-Style for loop reached 10!" << std::endl;
    else if(iter == 9)
      std::cout << "C-Style for loop reached 9!" << std::endl;

  }

  //
  // C++03 style for loop
  //

  for(int i = 0; i <= 5; i++)
  {
    //Will the code for i == 5 ever run? 
    if(i == 5)
      std::cout << "C++03 for loop reached 5!" << std::endl;
    else if(i == 4)
      std::cout << "C++03 for loop reached 4!" << std::endl;
  }

  //
  // Modern C++ for loop
  //

  int a[] = {1, 20, 56, 72, 98};

  //For all values i, in the sequence a...
  for(auto i : a)
  {
    std::cout << i << " ";
  }

  std::cout << std::endl;

  //return omitted because this function returns void
}

//
//
//
//
//

void while_loop(void)
{
  int iter = 0;

  //The less than operator
  while(iter < 10)
  {
    iter++;
  }

  std::cout << "The value of iter after the while loop is: " << iter << std::endl;

}

//
//
//
//
//

void do_while_loop(void)
{
  int iter = 0;

  do
  {
    iter++;

  }while(iter < 25);

  std::cout << "The value of iter after the do while loop is: " << iter << std::endl;

}

//
//
//
//
//

void infinite_loop(void)
{
  //Declare a string for input to be captured into
  std::string user_input;

  //Prompt the user with what to do to continue program execution
  std::cout << "type quit to exit" << std::endl;

  //Loop forever
  while(true)
  {
    //cin does the opposite of cout, it captures characters from the console
    //cin is 'blocking' -> this means that the code will stop here until the
    //function returns.  This can be both desireable and undesireable!!!
    std::cin >> user_input;

    //If the string entered is 'quit' we exit the loop
    if(user_input == "quit")
      //Break will quit the while loop
      break;
    else
      std::cout << "user entered: " << user_input << " instead of quit" << std::endl;
  }

  std::cout << "Exited the infinite loop!" << std::endl;

}

//
//
//
//
//


